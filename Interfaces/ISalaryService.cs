﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using V8API.Models;

namespace V8API.Interfaces
{
    public interface ISalaryService
    {
        List<Salary> GetSalaryByEmployee(Salary salary);

        List<Position> GetPositions();
        List<Division> GetDivision();
        List<Office> GetOffice();
        List<Salary> GetEmployeeList(Salary salary);
        Result SetSalary(Salary salary);
        Result DeleteSalary(Salary salary);
        List<Position> GetSalaryByEmployee();
        List<Salary> GetSalaryList(String EmployeeCode);

    }
}
