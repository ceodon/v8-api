﻿using System;
using System.Collections.Generic;
using System.Linq;
using V8API.Interfaces;
using V8API.Models;

namespace V8API.Service
{
    public class SalaryService : ISalaryService
    {
        private V8TestContext context;
        public SalaryService(V8TestContext Context)
        {
            context = Context;
        }

        public List<Salary> GetSalaryByEmployee(Salary salary)
        {
            return context.Salaries.Where(s=> s.EmployeeCode == salary.EmployeeCode) .OrderBy(e => e.Year).ThenBy(e => e.Month).ToList();            
        }

        public List<Salary> GetEmployeeList(Salary salary)
        {
            var employees = context.Salaries.OrderBy(e=>  e.Year).ThenBy(e=> e.Month).ToList().GroupBy(t => t.EmployeeCode).Select(e => e);
            var result = new List<Salary>();
            foreach (var item in employees.ToList())
            {
                result.Add(new Salary() { 
                    EmployeeCode = item.Key
                    ,EmployeeName =  item.Last().EmployeeName
                    ,EmployeeSurName = item.Last().EmployeeSurName
                });
            }
            return result;
        }

        public List<Position> GetSalaryByEmployee()
        {
            return context.Positions.ToList();
        }

        public List<Salary> GetSalaryList()
        {
            List<Salary> empList;
            try
            {
                empList = context.Set<Salary>().ToList();
            }
            catch (Exception)
            {
                throw;
            }
            return empList;
        }

        public List<Salary> GetSalaryList(string EmployeeCode)
        {
            throw new NotImplementedException();
        }

        public Result SetSalary(Salary salary)
        {
            var result = new Result();
            //Validation
            var office = context.Offices.Find(salary.OfficeId);
            if (office != null)
            {
                salary.Office = office;
            }
            else
            {
                result.Messsage = "Invalid Office";
                return result;
            }

            var position = context.Positions.Find(salary.PositionId);
            if (position != null)
            {
                salary.Position = position;
            }
            else
            {
                result.Messsage = "Invalid position";
                return result;
            }

            var division = context.Divisions.Find(salary.DivisionId);
            if (division != null)
            {
                salary.Division = division;
            }
            else
            {
                result.Messsage = "Invalid Division";
                return result;
            }
            if (salary.Id == 0)
            {
                context.Salaries.Add(salary);
            }
            else
            {
                context.Salaries.Update(salary);
            }
            
            context.SaveChanges();
            result.IsSuccess = true;
            result.Id = salary.Id;
            return result;
        }

        public List<Position> GetPositions()
        {
            return this.context.Positions.Where(p => p.Active == true).OrderBy(p=> p.Name).ToList();
        }

        public List<Division> GetDivision()
        {
            return this.context.Divisions.Where(p => p.Active == true).OrderBy(d => d.Name).ToList();
        }

        public List<Office> GetOffice()
        {
            return this.context.Offices.Where(p => p.Active == true).OrderBy(o => o.Name).ToList();
        }

        public Result DeleteSalary(Salary salary)
        {
            var result = new Result();
            var s = context.Salaries.Where(s => s.Id == salary.Id).FirstOrDefault();
            if (s.Id > 0)
            {
                context.Salaries.Remove(s);
                context.SaveChanges();
                result.IsSuccess = true;
            }
            return result;           
        }
    }
}
