﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace V8API.Models
{
    public partial class V8TestContext : DbContext
    {
        public V8TestContext()
        {
        }

        public V8TestContext(DbContextOptions<V8TestContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Division> Divisions { get; set; }
        public virtual DbSet<Office> Offices { get; set; }
        public virtual DbSet<Position> Positions { get; set; }
        public virtual DbSet<Salary> Salaries { get; set; }

//        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
//        {
//            if (!optionsBuilder.IsConfigured)
//            {
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
//                optionsBuilder.UseSqlServer("Server=DESKTOP-S0GMADT\\SQLEXPRESS;Database=V8Test;Trusted_Connection=True;");
//            }
//        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Modern_Spanish_CI_AS");

            modelBuilder.Entity<Division>(entity =>
            {
                entity.ToTable("Division");

                entity.Property(e => e.Active).HasDefaultValueSql("((1))");

                entity.Property(e => e.Name)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Office>(entity =>
            {
                entity.ToTable("Office");

                entity.Property(e => e.Active).HasDefaultValueSql("((1))");

                entity.Property(e => e.Name)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Position>(entity =>
            {
                entity.ToTable("Position");

                entity.Property(e => e.Active).HasDefaultValueSql("((1))");

                entity.Property(e => e.Name)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Salary>(entity =>
            {
                entity.ToTable("Salary");


                entity.Property(e => e.BaseSalary).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.BeginDate).HasColumnType("date");

                entity.Property(e => e.Birthday).HasColumnType("date");

                entity.Property(e => e.Commission).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.CompensationBonus).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.Contributions).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.EmployeeCode)
                    .HasMaxLength(10)
                    .IsFixedLength(true);

                entity.Property(e => e.EmployeeName)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.EmployeeSurName)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.IdentificationNumber)
                    .HasMaxLength(10)
                    .IsFixedLength(true);

                entity.Property(e => e.ProductionBonus).HasColumnType("decimal(18, 2)");

                entity.HasOne(d => d.Division)
                    .WithMany(p => p.Salaries)
                    .HasForeignKey(d => d.DivisionId)
                    .HasConstraintName("FK_Salary_Division");

                entity.HasOne(d => d.Office)
                    .WithMany(p => p.Salaries)
                    .HasForeignKey(d => d.OfficeId)
                    .HasConstraintName("FK_Salary_Office");

                entity.HasOne(d => d.Position)
                    .WithMany(p => p.Salaries)
                    .HasForeignKey(d => d.PositionId)
                    .HasConstraintName("FK_Salary_Position");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
