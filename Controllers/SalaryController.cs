﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using V8API.Interfaces;
using V8API.Models;
using V8API.ViewModels;

namespace V8API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class SalaryController : Controller
    {
        ISalaryService salaryService;
        public SalaryController(ISalaryService service)
        {
            salaryService = service;
        }


        [HttpPost]
        [Route("get-employee")]
        public EmployeeViewModel GetEmployeeList([FromBody] Salary inputSalary)
        {
            var model = new EmployeeViewModel();
            try
            {
                var data = salaryService.GetEmployeeList(inputSalary);
                model.employees = data;                
            }
            catch (Exception e)
            {
                model.IsValid = false;
                model.Message = e.Message;
            }
            return model;
        }

        [HttpPost]
        [Route("get-salary-by-employee")]
        public SalaryViewModel GetSalaryByEmployee([FromBody] Salary inputSalary)
        {
            var model = new SalaryViewModel();
            try
            {
                var data = salaryService.GetSalaryByEmployee(inputSalary);
                model.IsValid = true;
                model.salaryList = data;
            }
            catch (Exception e)
            {
                model.IsValid = false;
                model.Message = e.Message;
            }
            return model;
        }


        [HttpPost]
        [Route("set-salary")]
        public ResponseViewModel SetSalary([FromBody] Salary inputSalary)
        {
            var model = new ResponseViewModel();
            try            
            {
                var data = salaryService.SetSalary(inputSalary);
                model.IsValid = data.IsSuccess;
                model.Message = data.Messsage;
                model.Id = data.Id;
            }
            catch (Exception e)
            {
                model.IsValid = false;
                model.Message = e.Message;
            }
            return model;
        }

        [HttpPost]
        [Route("get-position")]
        public PositionViewModel GetPositions()
        {
            var model = new PositionViewModel();
            try
            {                
                model.positionList = salaryService.GetPositions(); ;
                model.IsValid = true;                
            }
            catch (Exception e)
            {
                model.IsValid = false;
                model.Message = e.Message;
            }
            return model;
        }
        [HttpPost]
        [Route("get-division")]
        public DivisionViewModel Getdivisions()
        {
            var model = new DivisionViewModel();
            try
            {
                model.divisionList = salaryService.GetDivision(); ;
                model.IsValid = true;
            }
            catch (Exception e)
            {
                model.IsValid = false;
                model.Message = e.Message;
            }
            return model;
        }

        [HttpPost]
        [Route("get-Office")]
        public OfficeViewModel GetOffices()
        {
            var model = new OfficeViewModel();
            try
            {
                model.officeList = salaryService.GetOffice(); ;
                model.IsValid = true;
            }
            catch (Exception e)
            {
                model.IsValid = false;
                model.Message = e.Message;
            }
            return model;
        }


        [HttpPost]
        [Route("delete-salary")]
        public ResponseViewModel DeleteSalary([FromBody] Salary inputSalary)
        {
            var model = new ResponseViewModel();
            try
            {
                var data = salaryService.DeleteSalary(inputSalary);
                model.IsValid = data.IsSuccess;
                model.Message = data.Messsage;                
            }
            catch (Exception e)
            {
                model.IsValid = false;
                model.Message = e.Message;
            }
            return model;
        }
    }
}