﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

#nullable disable

namespace V8API.Models
{
    public partial class Salary
    {        
        public int Id { get; set; }
        public int? Year { get; set; }
        public int? Month { get; set; }
        public int? OfficeId { get; set; }
        public string EmployeeCode { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeSurName { get; set; }
        public int? DivisionId { get; set; }
        public int? PositionId { get; set; }
        public int? Grade { get; set; }
        public DateTime? BeginDate { get; set; }
        public DateTime? Birthday { get; set; }
        public string IdentificationNumber { get; set; }
        public decimal? BaseSalary { get; set; }
        public decimal? ProductionBonus { get; set; }
        public decimal? CompensationBonus { get; set; }
        public decimal? Commission { get; set; }
        public decimal? Contributions { get; set; }

        public virtual Division Division { get; set; }
        public virtual Office Office { get; set; }
        public virtual Position Position { get; set; }
    }
}
