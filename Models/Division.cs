﻿using System;
using System.Collections.Generic;

#nullable disable

namespace V8API.Models
{
    public partial class Division
    {
        public Division()
        {
            Salaries = new HashSet<Salary>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public bool? Active { get; set; }

        public virtual ICollection<Salary> Salaries { get; set; }
    }
}
