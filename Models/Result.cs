﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace V8API.Models
{
    public class Result
    {
        public int Id { get; set; }

        public bool IsSuccess { get; set; }

        public string Messsage { get; set; }
    }
}
