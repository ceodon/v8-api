﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using V8API.Models;

namespace V8API.ViewModels
{
    public class EmployeeViewModel: ResponseViewModel
    {
        public List<Salary> employees { get; set; }
    }
}
