﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using V8API.Models;

namespace V8API.ViewModels
{
    public class SalaryViewModel: ResponseViewModel
    {
        public List<Salary> salaryList { get; set; }
    }
}
