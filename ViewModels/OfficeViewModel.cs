﻿using System.Collections.Generic;
using V8API.Models;

namespace V8API.ViewModels
{
    public class OfficeViewModel : ResponseViewModel
    {
        public List<Office> officeList { get; set; }
    }
}
