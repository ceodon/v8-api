﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace V8API.ViewModels
{
    public class ResponseViewModel
    {
        public int Id { get; set; }

        public bool IsValid { get; set; }

        public string Message { get; set; }
    }
}
