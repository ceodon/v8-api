﻿using System.Collections.Generic;
using V8API.Models;

namespace V8API.ViewModels
{
    public class PositionViewModel : ResponseViewModel
    {
        public List<Position> positionList { get; set; }
    }
}
