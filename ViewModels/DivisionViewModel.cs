﻿using System.Collections.Generic;
using V8API.Models;

namespace V8API.ViewModels
{
    public class DivisionViewModel : ResponseViewModel
    {
        public List<Division> divisionList { get; set; }
    }
}
